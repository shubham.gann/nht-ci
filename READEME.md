# Shubham Raghuram Gannamraju - CI Assignment


## Jenkins Pipeline

- Used a lightweight image of Jenkins (liatrio/jenkins-alpine) which I found online, created for docker builds and deployments.

- Mounted the docker socket onto the cotainer to use docker 
"-v /var/run/docker.sock:/var/run/docker.sock"

- For faster testing, I was using a local git repo on my machine to test the Jenkins Pipeline, that
was done by mounting the git repo onto the container
"-v /home/srg/..../jenkins-cicd/nht-ci/node-hello-world:/node-hello-world"

- To ensure the pipeline is build on a commit on the repo, I used Jenkins SCM Polling to constantly
check for changes.

- Used the credentials section to add DockerHub credentials, and used it the environment section in
the Jenkins file

- DockerHub Link https://hub.docker.com/repository/docker/srg2/nht/general

Screenshots:

![Git SCM](<./screenshots/Screenshot from 2023-08-22 21-14-36.png>)

![Build Trigger](<./screenshots/Screenshot from 2023-08-22 21-14-41.png>)

![Successful Build](<./screenshots/Screenshot from 2023-08-22 21-14-18.png>)

![Build Success](<./screenshots/Screenshot from 2023-08-22 21-14-11.png>)

![DockerHub](./screenshots/dockerhub.png)

---

## Gitlab CI

- Setup of Gitlab Runner to run on local machine was done
- Realized need to execute the runner in shell mode to allow docker command inside the gitlab-ci file
- The build, test, package, deploy stages were made in the Ci file, each carrying out a subsection of the task
- Since the files are in the subfolder 'tcp-echo-server' a cd command before each stage was added
- The jar file created as an artifact in the package stage so that it is available to the next docker stage
- The pipeline finally pushes the docke image to DockerHub
- The Pipeline is triggered automatically on commit
- DockerHub Link https://hub.docker.com/repository/docker/srg2/nht/general

![Gitlab Runner](./screenshots/image.png)

![Runner on Gitlab](/screenshots/image-1.png)

![Successful Pipeline](/screenshots/image-2.png)

![Final Job](/screenshots/image-3.png)

![DockerHub](./screenshots/dockerhub.png)

---